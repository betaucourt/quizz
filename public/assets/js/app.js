/*
 * Welcome to your app's main JavaScript file!
*
* We recommend including the built version of this JavaScript file
* (and its CSS file) in your base layout (base.html.twig).
*/

// any CSS you require will output into a single css file (app.css in this case)
//require('../css/app.css');


function delete_answer(value)
{
    $("li[class$='"+value+" list-group-item list-group-item-action disabled d-flex justify-content-between align-items-center']").remove();
}

function add_answer()
{
    var index = $(".test ").find("li").length;
    console.log(index);
    index++;
    $(".list").append('<li class="'+index+' list-group-item list-group-item-action disabled d-flex justify-content-between align-items-center">Réponse: <input id="collection_answer_answer_'+index+'_value" class="titikaka form-control" type="text" name="collection_answer[answer]['+index+'][value]" required="false" value="">' +
        '<br><input class="btn btn-secondary" type="button" value="delete" id="answer_delete" name="'+index+'" onclick="delete_answer('+index+');"></li class="'+index+'">');
}

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// var $ = require('jquery');

console.log('Hello Webpack Encore! Edit me in assets/js/app.js');
