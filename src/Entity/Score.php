<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Boolean;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ScoreRepository")
 */
class Score
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(name="total_done", type="integer")
     */
    private $totalDone;

    /**
     * @ORM\Column(name="total_win", type="integer")
     */
    private $totalWin;

    /**
     * @ORM\Column(name="last_done", type="datetime")
     */
    public $lastDone;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="scores")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Question", inversedBy="score", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $question;

    public function __construct(Question $question, User $user)
    {
        $this->totalWin = 0;
        $this->totalDone = 0;
        $this->question = $question;
        $this->lastDone = new \DateTime('now');
        $this->user = $user;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTotalDone(): ?int
    {
        return $this->totalDone;
    }

    public function setTotalDone(int $totalDone): self
    {
        $this->totalDone = $totalDone;

        return $this;
    }

    public function getTotalWin(): ?int
    {
        return $this->totalWin;
    }

    public function setTotalWin(int $totalWin): self
    {
        $this->totalWin = $totalWin;

        return $this;
    }

    public function getLastDone(): ?\DateTimeInterface
    {
        return $this->lastDone;
    }

    public function setLastDone(\DateTimeInterface $lastDone): self
    {
        $this->lastDone = $lastDone;

        return $this;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getQuestion(): ?Question
    {
        return $this->question;
    }

    public function setQuestion(?Question $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function updateTotalDone()
    {
        $this->totalDone++;
    }

    public function updateTotalWin()
    {
        $this->totalWin++;
    }

    /**
     * @param bool $win
     */
    public function update(Bool $win)
    {
        if ($win == true) {
            $this->updateTotalWin();
            $this->user->updateTotalWin();
        }
        $this->user->updateTotalDone();
        $this->setLastDone(new \DateTime('now'));
        $this->updateTotalDone();
    }
}
