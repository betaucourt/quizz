<?php
// src/Entity/User.php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Score", mappedBy="user", cascade={"persist"}, orphanRemoval=true, fetch="EAGER")
     */
    private $scores;

    /**
     * @ORM\Column(name="total_done", type="integer")
     */
    private $totalDone;

    /**
     * @ORM\Column(name="total_win", type="integer")
     */
    private $totalWin;

    public function __construct()
    {
        parent::__construct();
        $this->scores = new ArrayCollection();
        $this->totalDone = 0;
        $this->totalWin = 0;
        // your own logic
    }

    /**
     * @return Collection|Score[]
     */
    public function getScores(): Collection
    {
        return $this->scores;
    }

    public function addScore(Score $score): self
    {
        if (!$this->scores->contains($score)) {
            $this->scores[] = $score;
            $score->setUser($this);
        }

        return $this;
    }

    public function removeScore(Score $score): self
    {
        if ($this->scores->contains($score)) {
            $this->scores->removeElement($score);
            // set the owning side to null (unless already changed)
            if ($score->getUser() === $this) {
                $score->setUser(null);
            }
        }

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTotalDone(): ?int
    {
        return $this->totalDone;
    }

    public function setTotalDone(int $totalDone): self
    {
        $this->totalDone = $totalDone;

        return $this;
    }

    public function getTotalWin(): ?int
    {
        return $this->totalWin;
    }

    public function setTotalWin(int $totalWin): self
    {
        $this->totalWin = $totalWin;

        return $this;
    }

    public function updateTotalDone(): self
    {
        $this->totalDone++;
        return $this;
    }

    public function updateTotalWin(): self
    {
        $this->totalWin++;
        return $this;
    }

}