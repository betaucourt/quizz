<?php

namespace App\Form;

use App\Entity\Question;
use App\Form\Type\AnswerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Class CollectionAnswerType
 * @package App\Form
 */
class CollectionAnswerType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextareaType::class, [
                'attr' => [
                    'class'
                ]
            ])
            ->add('description', TextareaType::class, [

            ])
            ->add('answer', CollectionType::class, [
                'entry_type' => AnswerType::class,
                'entry_options' => [
                    'label' => false,
                ],
                'attr' => [
                    'required' => false,
                ],
                'by_reference' => false,
                'allow_add' => true,
                'allow_delete' => true,
            ])
            ->add('save', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary'
            ],
                'label' => 'Sauvegarder et choisir la bonne réponse',
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Question::class,
            // Configure your form options here
        ]);
    }
}
