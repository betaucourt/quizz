<?php

namespace App\Repository;

use App\Entity\Question;
use App\Entity\Score;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Score|null find($id, $lockMode = null, $lockVersion = null)
 * @method Score|null findOneBy(array $criteria, array $orderBy = null)
 * @method Score[]    findAll()
 * @method Score[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScoreRepository extends ServiceEntityRepository
{
    /**
     * ScoreRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Score::class);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function findHowManyScore(User $user)
   {
       $query = $this->createQueryBuilder('s')
           ->select('COUNT(s.user)')
           ->where('s.user = :user')
           ->setParameter('user', $user->getId())
           ->getQuery();
       $ret = $query->execute();
       return $ret[0][1];
   }

    /**
     * @param User $user
     * @return mixed
     */
    public function findDoneScore(User $user)
   {
       $query = $this->createQueryBuilder('s')
           ->select('s')
           ->where('s.user = :user')
           ->setParameter('user', $user->getId())
           ->getQuery();
       $question_id = $query->execute();
       return $question_id;
   }

    function date_sort($a, $b) {
        return strtotime($a) - strtotime($b);
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function findQuestion(User $user)
   {
       $query = $this->createQueryBuilder('s')
           ->select('s')
           ->andWhere('s.user = :user')
           ->setParameter('user', $user->getId())
           ->orderBy('s.lastDone', 'ASC')
           ->setMaxResults(1)
           ->getQuery();
       $ret = $query->getOneOrNullResult();
       if ($ret->getLastDone() < new \DateTime('-1 week'))
           return $ret;
       else {
           $query = $this->createQueryBuilder('s')
               ->select('s')
               ->andWhere('s.user = :user')
               ->setParameter('user', $user->getId())
               ->orderBy('(s.totalWin) / (s.totalDone)', 'ASC')
               ->addOrderBy('s.lastDone', 'ASC')
               ->setMaxResults(1)
               ->getQuery();
       }
       return $query->getOneOrNullResult();
   }

//    /**
//     * @return Score[] Returns an array of Score objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Score
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
