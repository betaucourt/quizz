<?php

namespace App\Repository;

use App\Entity\Question;
use App\Entity\Score;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Question|null find($id, $lockMode = null, $lockVersion = null)
 * @method Question|null findOneBy(array $criteria, array $orderBy = null)
 * @method Question[]    findAll()
 * @method Question[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class QuestionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Question::class);
    }

    /**
     * @param $user
     * @return mixed
     */
    public function findHowManyQuestion($user)
    {
        $query = $this->createQueryBuilder('q')
            ->select('COUNT(q.id)')
            ->getQuery();
        $ret = $query->execute();
        return $ret[0][1];
    }

    /**
     * @param $scores
     * @return Question|null
     */
    public function findRandomQuestion($scores)
    {
        $query = $this->createQueryBuilder('q')
            ->select('q.id')
            ->getQuery();
        $question = $query->execute();
        foreach ($question as $question_id)
            $tmp_question[] = $question_id['id'];

        if ($scores == null) {
            $ret = array_rand($question);
            return $this->find($question[$ret]);
        }
        foreach ($scores as $score)
            $tmp[] = $score->getQuestion()->getId();
        $result = array_diff(
            $tmp_question, $tmp);

        $final = array_rand($result);
        $return = $this->find($result[$final]);
        return $return;
   }

//    /**
//     * @return Question[] Returns an array of Question objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('q.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Question
    {
        return $this->createQueryBuilder('q')
            ->andWhere('q.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
