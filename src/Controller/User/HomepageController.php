<?php

namespace App\Controller\User;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class HomepageController
 * @package App\Controller\user
 */
class HomepageController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function homepage()
    {
        return $this->redirectToRoute('homepage_lang');
    }

    public function homepage_lang()
    {
        $user = $this->getUser();
        if ($user == null)
            return $this->redirectToRoute('fos_user_security_login', [

            ]);
        else {
            $session = $this->get('session');
            $session->set('nb_win', 0);
            $session->set('nb_done', 0);
            return $this->redirectToRoute('user', [

            ]);
        }

    }
}
