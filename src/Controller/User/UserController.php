<?php

namespace App\Controller\User;

use App\Entity\Answer;
use App\Entity\Question;
use App\Entity\Score;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
/**
 * Class UserController
 * @package App\Controller\user
 */
class UserController extends AbstractController
{
    //public $manager;


   /* public function __construct()
    {
        $this->manager = $this->getDoctrine()->getManager();
    }*/
    public function index()
    {
        return $this->render('user/index.html.twig');
    }


    public function startQuizz(Request $request)
    {
        $entity_manager = $this->getDoctrine()->getManager();
        $score = $this->findAScore($entity_manager);
        $session = $this->get('session');
        $q_done = $session->get('nb_done');
        $q_win = $session->get('nb_win');
        if ($score == null) {
            $this->addFlash('notice', 'Il n\'y a pas encore de questions faite par l\'administrateur');
            return $this->render('user/index.html.twig');
        }
        $question = $score->getQuestion();
        $answers = $this->getDoctrine()->getRepository(Answer::class)->findBy(['question' => $score->getQuestion()->getId()]);
        if ($request->isMethod('POST')) {
            $question_list = $request->request->all();
            foreach ($answers as $right_answer)
                if ($question_list['answer'] == $right_answer->getId())
                    return $this->checkQuestion($answers, $right_answer, $score, $entity_manager);

            return $this->checkQuestion($answers, new Answer(), $score, $entity_manager);
        }

        if ($q_done >= $this->getParameter('nbQuestion')) {
            $session->set('nb_done', 0);
            $session->set('nb_win', 0);
            return $this->render('user/quizz/final.html.twig', [
                'done' => $q_done,
                'win' => $q_win,
            ]);
        }
        else {
            return $this->render('user/quizz/question.html.twig', [
                'place' => $session->get('nb_done'),
                'question' => $question,
                'answers' => $answers,
            ]);
        }
    }

    public function findAScore()
    {
        $user = $this->getUser();

        if ( $this->diffScoreQuestion() > 0) {
            $question_id = $this->getDoctrine()->getRepository(Score::class)
                ->findDoneScore($user);
            $question = $this->getDoctrine()->getRepository(Question::class)->findRandomQuestion($question_id);
            $score = new Score($question, $user);
            return $score;
        } else {
            $score = $this->getDoctrine()->getRepository(Score::class)
                ->findQuestion($user);
            return $score;
        }
    }

    public function diffScoreQuestion()
    {
        $user = $this->getUser();

        $nb_score = $this->getDoctrine()->getRepository(Score::class)->findHowManyScore($user);
        $nb_question = $this->getDoctrine()->getRepository(Question::class)->findHowManyQuestion($user);

        return $nb_question - $nb_score;
    }

    public function checkQuestion($answers, Answer $answer, Score $score, $entity_manager)
    {
        $session = $this->get('session');
        if ($answer->getIsTrue() == 1) {
            $score->update(true);
            $session->set('nb_win', $session->get('nb_win') + 1);
            $session->set('nb_done', $session->get('nb_done') + 1);
            $entity_manager->persist($score);
            $entity_manager->flush();
            $this->addFlash('win', $this->getGoodAnswer($answers));
            return $this->redirectToRoute('start_quizz', [
            ]);
        } else {
            $score->update(false);
            $entity_manager->persist($score);
            $entity_manager->flush();
            $this->addFlash('loose', $this->getGoodAnswer($answers));
            return $this->redirectToRoute('start_quizz', [
            ]);
        }
    }

    public function getGoodAnswer($answers)
    {
        foreach ($answers as $answer) {
            if ($answer->getIsTrue() == 1)
                return $answer->getValue();
        }
    }

    public function noQuestion()
    {
        $this->addFlash('danger', 'Il n\'y a pas encore de questions faite par l\'administrateur');
        return $this->render('user/index.html.twig');
    }

}
