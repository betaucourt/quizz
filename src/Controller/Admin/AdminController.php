<?php

namespace App\Controller\Admin;

use App\Form\AddQuestionType;
use App\Form\CollectionAnswerType;
use App\Entity\Question;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminController
 * @package App\Controller\admin
 */
class AdminController extends Controller
{

    /**
     * @param $entity_manager
     * @param Request $request
     * @return mixed
     */
    public function getPagination($entity_manager, Request $request)
    {
        $appontmentsRepository = $entity_manager->getRepository(Question::class);
        $allappointmentsQuery = $appontmentsRepository->findAll();
        $paginator = $this->get('knp_paginator');
        $appointments = $paginator->paginate(
            $allappointmentsQuery,
            $request->query->getInt('page', 1),
            $this->getParameter('pagination')
        );
        return $appointments;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        $entity_manager = $this->getDoctrine()->getManager();

        $questions = $this->getDoctrine()->getRepository(Question::class)->findAll();
        $new_question = new Question();
        $new_question_form = $this->createForm(AddQuestionType::class, $new_question);
        $new_question_form->handleRequest($request);


        $pagination = $this->getPagination($entity_manager, $request);
        if ($new_question_form->isSubmitted() && $new_question_form->isValid()) {
            $new_question = $new_question_form->getData();
            $entity_manager->persist($new_question);
            $entity_manager->flush();
            return $this->redirectToRoute('question', ['id' => $new_question->getId()]);
        }
        return $this->render('admin/index.html.twig', [
            'questions' => $questions,
            'newQuestion' => $new_question_form->createView(),
            'appointments' => $pagination,
        ]);
    }

    /*
    *  @ParamConverter("question", class="App:Question")
    */
    public function delete(Question $question)
    {
        $this->getDoctrine()->getManager()->remove($question);
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('admin');
    }

    /*
     *  @ParamConverter("question", class="App:Question")
     */
    /**
     * @param Request $request
     * @param Question|null $question
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function questionList(Request $request, Question $question = null)
    {
        if (empty($question))
            return $this->redirectToRoute('admin');

        $entity_manager = $this->getDoctrine()->getManager();

        $form = $this->createForm(CollectionAnswerType::class, $question);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $question = $form->getData();
            $entity_manager->persist($question);
            $entity_manager->flush();
            return $this->redirectToRoute('select_answer', ['id' => $question->getId()]);
        }
        return $this->render('admin/question.html.twig', [
            'form' => $form->createView(),
            'question_id' => $question->getId(),

        ]);
    }

    /*
     *  @ParamConverter("question", class="App:Question")
     */
    /**
     * @param Request $request
     * @param Question|null $question
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function SelectAnswer(Request $request, Question $question = null)
    {
        $entity_manager = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
            $result = $request->request->all();
            $question->selectGoodAnswer($result['select']);

            $entity_manager->persist($question);
            $entity_manager->flush();
            return $this->redirectToRoute('admin');
        }
        return $this->render('admin/select_answer.html.twig', [
            'question' => $question,
            'answers' => $question->getAnswer(),
        ]);
    }
}
